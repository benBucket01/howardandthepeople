class LoseScreen{
  private PImage loseScreen_notPlayAgain; // PImage for the losing screen with not highlighted button
  private PImage loseScreen_PlayAgain; // PImage for the losing screen with a hoghlighted button
  private boolean playAgain; // This boolean will determin if the player will play again
  
  // Losing Screen contructor
  LoseScreen(){
    playAgain = false; // Set to false because player as not made desision
    loseScreen_notPlayAgain = loadImage("loseScreen_notPlayAgain.png"); // Load this image from data folder
    loseScreen_PlayAgain = loadImage("loseScreen_PlayAgain.png"); // Load this image from data folder
  }
  
  //The display method will display the losing screens at the correct time 
  public void display(){
    if(mouseX > 50 && mouseX < 370 && mouseY > 320 && mouseY < 375){ // If we are inside a certain area
      image(loseScreen_PlayAgain,0,0,width,height); // Then load the Play again highlighted area
      playAgain = true; // We can now play again so set to true
    } else { // If we are anywhere on the screen other then inside that area
      image(loseScreen_notPlayAgain,0,0,width,height); // Display the losing screen but without the highlighted area
      playAgain = false; // The player as not desided to play again so we set this to false
    }
  }
  
  // This method allows us to return the playAgain boolean
  public boolean getPlayAgain(){
    return playAgain; // Return the playAgain boolean
  }
  
}
