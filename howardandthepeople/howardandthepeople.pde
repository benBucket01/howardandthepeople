/*
Author: Benjamin, Kyle, Rui, Zizo, Du
Date: 2019-12-09
Description: This is a game called howard and the people. You play as howard who as to dodge the evil rectangles while collecting coins
*/
//Import sound from data folder
import processing.sound.*;

//Import playerMovement Class
Player player;
TitleScreen titleScreen;
LoseScreen loseScreen;

Box boxGrabable[] = new Box[1];
Box boxStatic[] = new Box[1];

//Setting up diameters for the rectangles I want to create.
float centerRectX = 150;
float centerRectY = 150;
float centerRectWidth = 50;
float centerRectHeight = 50;
float mouseRectWidth = 40;
float mouseRectHeight = 40;

PImage cloud;

int time;
int wait = 1000;
int realTime;

int stage;

//Setting up the end condition class
//End end = new End();

levelManager levelManager;


//For Finding the song file
SoundFile backgroundSong;
String backgroundSongName = "data/background.wav";
String backgroundSongPath;

void setup() {
  titleScreen = new TitleScreen();
  loseScreen = new LoseScreen();
  stage = 0;
  //Play the sound file in that location
  backgroundSongPath = sketchPath(backgroundSongName);
  backgroundSong = new SoundFile(this, backgroundSongPath);
  backgroundSong.amp(0.3);
  backgroundSong.play();
  cloud = new PImage();
  cloud = loadImage("The Forest.png");
  cloud.resize(700,710);
  time = millis();

  size(500, 500);
  levelManager = new levelManager();
}

//Draw the program 
void draw() {
  if(stage == 0){
    titleScreen.display();
  } else if(stage == 1){
    image(cloud,-110,-210);

  if (millis() - time >= wait) {
    realTime++;
    //println(realTime);
    if (realTime == 360) {
      backgroundSong.play();
      realTime = 0;
    }
    time = millis();
  }

  //println("DRAW STARTS");
  levelManager.display();
  
  if(levelManager.isRetart == true){
    stage = 2;
    levelManager.isRetart = false;
  }
  
  } else if(stage == 2){
    loseScreen.display();
  }
}


public void mouseDragged() {
  levelManager.dragged();
}

public void mousePressed() {
  levelManager.pressed();
  if(titleScreen.getIsStartable() == true){
    stage = 1;
  } else if(loseScreen.getPlayAgain() == true){
    stage = 0;
  }
}

public void mouseReleased() {
  levelManager.released();
}


//Player moves when the key is pressed (wasd)
void keyPressed() {
  levelManager.kPressed();
}

//Player stops moving when the key is released (wasd)
void keyReleased() {
  levelManager.kReleased();
}
