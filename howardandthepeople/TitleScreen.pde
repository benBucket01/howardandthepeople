class TitleScreen{
  
  private PImage titleImage_notStart; // Create a PImage of a title screen with the non-highlighted button
  private PImage titleImage_Start; // Create a PImage of a title screen with the highlighted button
  private boolean isStartable; // Boolean to determin if the game is startable
  
  //Title screen contructor
  TitleScreen(){
    titleImage_notStart = loadImage("TitleScreen_NewOff_IntroOff.png"); // load image for non_start title screen
    titleImage_Start = loadImage("TitleScreen_NewOn_IntroOff.png"); // load image for start title screen
    isStartable = false; // the game as not started yet so we set is startable to false
  }
  
  
  //The display method will display the losing screens at the correct time 
  private void display(){
    if(mouseX > 260 && mouseX < 490 && mouseY > 390 && mouseY < 450){ //If we are inside a certain area
      image(titleImage_Start,0,0,width,height); // load title start image
      isStartable = true; // startable is true
    } else{ // If we are anywhere on the screen other then inside that area
      image(titleImage_notStart,0,0,width,height); // load title non start image
      isStartable = false; // startable is false
    }
  }
  
  //This method will return the isStartable boolean
  public boolean getIsStartable(){
    return isStartable; // return the isStartable boolean
  }
}
