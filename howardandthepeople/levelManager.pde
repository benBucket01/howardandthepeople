class levelManager {
  Player player;
  private float movementSpeed; // This variable will determin the speed at which the level will move in
  private float Distance; // this variable will keep track of the players distance traveled
  private float timer = 0;
  private int boxGrab = 1;
  Box boxGrabable[] = new Box[1];
  private ArrayList <bullet> bullet;
  private ArrayList <life> life;
  private ArrayList <Coin> coin;
  private float lifetime;
  private float coinCount;
  private int coinAmmount;
  public boolean isRetart;
  
  levelManager() {
    coinAmmount = 0;
    movementSpeed = 1;
    player = new Player();
    bullet = new ArrayList<bullet>();
    life = new ArrayList<life>();
    coin = new ArrayList<Coin>();
    boxGrabable[0] = new Box(player,250,250,100,20,true);
  }

  public void display() {
    timer();
    Distance += 0.1;


    textSize(20);
    text("LIFE: " + player.getLife(), 280, 20);
    text("DISTANCE: " + Distance, 280, 50);
    textSize(20);


    textSize(20);
    fill(255);
    text("LIFE: " + player.getLife(),280,20);
    text("DISTANCE: " + Distance,280,50);
    text("COINS: " + coinAmmount, 280,80);
    for (int i = 0; i < boxGrabable.length; i++) {
      boxGrabable[i].display();
    }
    bullet b;
    for (int i = 0; i < bullet.size(); i++) {
      b = bullet.get(i);
      b.display();
    }

    life lf;
    for (int i = 0; i < life.size(); i++) {
      lf = life.get(i);
      lf.display();
    }

    Coin co;
    for (int c = 0; c < coin.size(); c++) {
      co = coin.get(c);
      co.display();
    }

    removeBullets();
    bulletHitPlayer();
    coinHitPlayer();

    player.display();
    restart();
    if (timer <= 0) {
      startFireing();
      timer = 20;
    }
    if (lifetime <= 0) {
      lifespawn();
      lifetime = 100;
    }
    if (coinCount <= 0) {
      spawnCoin();
      coinCount = 100;
    }
  }

  private void coinHitPlayer() {
    Coin c;
    for (int i = 0; i < coin.size(); i++) {
      c = coin.get(i);
      if (c.getPosition().x > player.getPosition().x && c.getPosition().x < player.getPosition().x + 50 && c.getPosition().y > player.getPosition().y && c.getPosition().y < player.getPosition().y + 50) {
        coin.remove(c);
        coinAmmount += 1;
      }
    }
  }

  private void bulletHitPlayer() {
    bullet b;
    for (int i = 0; i < bullet.size(); i++) {
      b = bullet.get(i);
      if (b.getPosition().x > player.getPosition().x && b.getPosition().x < player.getPosition().x + 50 && b.getPosition().y > player.getPosition().y && b.getPosition().y < player.getPosition().y + 50) {
        bullet.remove(b);
        player.setLife(player.getLife() - 1);
      }
    }
  }

  private void removeBullets() {
    bullet b;
    for (int i = 0; i < bullet.size(); i++) {
      b = bullet.get(i);
      if (b.getPosition().x <= 0) {
        bullet.remove(b);
      }
    }
  }


  private void restart() {
    if (player.getLife() <= 0 || player.getPosition().y + 50 > height) {
      bullet b;
      for (int i = 0; i < bullet.size(); i++) {
        b = bullet.get(i);
        bullet.remove(b);
      }
      Coin c;
      for (int ic = 0; ic < coin.size(); ic++) {
        c = coin.get(ic);
        coin.remove(c);
      }
      
      boxGrabable[0].Position = new PVector(250,240);
      coinAmmount = 0;
      timer = 0;
      Distance = 0;
      player.setLife(3);
      player.reset();
      isRetart = true;
    }
  }


  private void spawnCoin() {
    if (coinCount == 0) {
      float randomPosition = random(0, 500);
      float randomSpeed = random(0, 4);
      coin.add(new Coin((int)randomPosition, (int)randomSpeed));
    }
  }

  public void startFireing() {
    float randomPosition = random(0, 500);
    float randomSpeed = random(0, 4);
    bullet.add(new bullet((int)randomPosition, (int)randomSpeed));
  }

  public void lifespawn() {
    float randomPosition = random(0, 500);
    float randomSpeed = random(0, 2);
    life.add(new life((int)randomPosition, (int)randomSpeed));
  }

  private void timer() {
    timer -= 1;
    coinCount -= 1;
    lifetime -= 1;
  }


  public void dragged() {
    for (int i = 0; i <  boxGrabable.length; i++) {
      boxGrabable[i].grabber.grabberDragged();
    }
  }

  public void pressed() {
    for (int i = 0; i <  boxGrabable.length; i++) {
      boxGrabable[i].grabber.grabberPressed();
    }
    life lf;
    for (int i = 0; i < life.size(); i++) {
      lf = life.get(i);
      if (mouseX >= lf.getPosition().x && mouseX <= lf.getPosition().x + 20 && mouseY >= lf.getPosition().y && mouseY <= lf.getPosition().y + 20) {
        life.remove(lf);
        player.setLife(player.getLife() + 1);
      }
    }
  }


  public void released() {
    for (int i = 0; i <  boxGrabable.length; i++) {
      boxGrabable[i].grabber.grabberReleased();
    }
  }

  public void kPressed() {
    player.movement();
  }


  public void kReleased() {
    player.stopPlayer();
  }
}
