class bullet {
  private PImage bullet[];
  private PVector Position;
  private int movementSpeed;

  bullet(int y, int movementSpeed) {
    this.movementSpeed = movementSpeed;
    Position = new PVector(500, y);
  }

  public void display() {
    Position.sub(movementSpeed, 0);
    bullet = new PImage[1];
    
    bullet[0] = loadImage("The Wood.png");
    
    fill(255, 0, 0);
    rect(Position.x, Position.y, 20, 20);
  }
 

  public PVector getPosition() {
    return Position;
  }
}
