class Coin{
  private PVector Position;
  private int movementSpeed;
  
  private PImage coin[];
  
  Coin(int y, int movementSpeed){
    this.movementSpeed = movementSpeed;
        Position = new PVector(500, y);
    
    coin = new PImage[4];
    
    coin[0] = loadImage("The Coin (1).gif");
    coin[1] = loadImage("The Coin (2).gif");
    coin[2] = loadImage("The Coin (3).gif");
    coin[3] = loadImage("The Coin (4).gif");
    
    for(int i = 0; i < coin.length; i++){
      coin[i].resize(40,40);
    }
  }
  
  public void display(){
    Position.sub(movementSpeed,0);
    image(coin[frameCount%4],Position.x,Position.y);
  }
  
  public PVector getPosition(){
    return Position;
  }
}
