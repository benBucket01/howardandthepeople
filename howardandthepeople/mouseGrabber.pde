class mouseGrabber {
  private boolean isGrabbed; // This variable will say if the object is grabbed
  private PVector position; // THis is the position of said object being grabbed
  private int countDown; // This is a varaiable that will determin the count down clock
  private int Width;
  private int Height;
  private boolean grabbed;

  private boolean isInsideObject;

  //mouseGrabber constructor
  mouseGrabber(PVector position, int Width, int Height) {
    this.Width = Width;
    this.Height = Height;
    this.position = position; //Setup the postion
    countDown = 100; // Set the count down clock
    isGrabbed = false; // We have not grabbed object so we set isGrabbed to false
    isInsideObject = false;
  }

  public void isMouseInsideObject(PVector Position) {
    if (mouseX >= Position.x && mouseX <= Position.x + Width && mouseY >= Position.y && mouseY <= Position.y + Height + 20) {
      isInsideObject = true;
    } else {
      isInsideObject = false;
    }
  }

  public void grabber(PVector Position) {
    isMouseInsideObject(Position);
    if (grabbed == true) {
      startCountDown();
    }
    if (countDown <= 0) { // If the countdown is 0 
      grabbed = false; // The game object is now being grabbed anymore
    } 
    if (grabbed == false) { // If the game object is not being grabbed
      countDown = 100; // the count down is set to 100
    }
  }





  public void grabberPressed() {
    if (isInsideObject == true) {
      grabbed = true;
    }
  }

  public void grabberDragged() {
    if (grabbed == true) {
      position.x = mouseX - (Width/2); // Set the position of the y position
      position.y = mouseY - (Height/2); // Set the position of the y position
    }
  }

  public void grabberReleased() {
    grabbed = false;
  }

  //This will the position 
  public void setPosition(PVector position) {
    this.position = position;
  }

  //This will start lowering the countDown variable 
  private void startCountDown() {
    countDown -= 1;
  }

  //This will get the coutDown variable
  public int getCountDown() {
    return countDown;
  }

  public boolean getIsGrabbed() {
    return grabbed;
  }
}
