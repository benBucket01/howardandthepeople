class Box {
  private Player player; // Create player object
  private mouseGrabber grabber; // Create mouseGrabber object
  public PVector Position; // This is the position of the wall
  private int Height, Width; // These are the height and width variables
  private boolean isGrabable;
  private float movementSpeed;
  private PImage cloud;

  //Box constructor
  Box(Player player, int x, int y, int Width, int Height, boolean isGrabable) {
    this.isGrabable = isGrabable;
    this.player = player; // Setup the player
    Position = new PVector(x, y); // Set the position
    grabber = new mouseGrabber(Position, Width, Height); // Setup the grabber
    this.Width = Width; // Setup the width
    this.Height = Height; // Setup the height
    
    cloud = new PImage();
    cloud = loadImage("The Cloud 1.png");
  }


  //This method will display the box 
  public void display() {
    if (isGrabable == true) {
      //println("IS GRABABLE WAS TRUE");
      grabber.grabber(Position);
      //If the wall is grabed
      if (grabber.getIsGrabbed() == true) {
        fill(200 - grabber.getCountDown(), grabber.getCountDown(), 0); // Fill the object a certain color
        grabber.setPosition(Position); // Set the position of the box
      } else if (grabber.getIsGrabbed() == false) { // If the wall is not grabed
        fill(255); // Fill the wall white
        player.setOnObject(false); // Player is not on object

        //If the player is on the wall
        if (player.getPosition().x + 50 >= Position.x && player.getPosition().x <= Position.x + Width && player.getPosition().y + 35 >= Position.y && player.getPosition().y + 50 <= Position.y + Height
          && player.getIncreaseSpeed() >= 1) {
          player.getPosition().y = Position.y - 30; // Set the player position
          player.setOnObject(true); // The player is on the object
        }
      }
    } else if (isGrabable == false) {
      //println("IS GRABABLE WAS FALSE");
      fill(255);
      if (player.getPosition().x + 50 >= Position.x && player.getPosition().x <= Position.x + Width && player.getPosition().y + 50 >= Position.y && player.getPosition().y + 50 <= Position.y + Height
        && player.getIncreaseSpeed() >= 1) {
        //println("PLAYER IS NOW INSIDE BOX");
        player.getPosition().y = Position.y - 50; // Set the player position
        player.setOnObject(true); // The player is on the object
        //println("FUNCTION HAS ENDED");
      }
    }

    
    Position.sub(new PVector(movementSpeed, 0));

    image(cloud, Position.x, Position.y);
    fill(0);
  }

  public void setMovementSpeed(float speed) {
    movementSpeed = speed;
  }
}
