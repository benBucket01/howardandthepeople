
class Player {
  private PVector Position; // Position of player
  private float gravity; // Gravity variable
  public float increaseSpeed; // This variable is used to simulate gravity
  private boolean left, right; // Boolean that determins if the player will move left or right
  private float movementSpeed = 5; // Movement speed that the player can move at
  private boolean canJump; // Boolean that determins if the player can jump
  private boolean onObject; // Boolean that determins if the player is on an object
  private boolean Jump;
  private int life = 3;

  private mouseGrabber grabber; // Makes the object grabable

  PImage[] walk;
  PImage[] grab;
  PImage[] stand;
  PImage[] jump;

  //Players contructor
  Player() {
    walk = new PImage[16];
    walk[0] = loadImage("Poro Walk (1).gif");
    walk[1] = loadImage("Poro Walk (2).gif");
    walk[2] = loadImage("Poro Walk (3).gif");
    walk[3] = loadImage("Poro Walk (4).gif");
    walk[4] = loadImage("Poro Walk (5).gif");
    walk[5] = loadImage("Poro Walk (6).gif");
    walk[6] = loadImage("Poro Walk (7).gif");
    walk[7] = loadImage("Poro Walk (8).gif");
    walk[8] = loadImage("Poro Walk (9).gif");
    walk[9] = loadImage("Poro Walk (10).gif");
    walk[10] = loadImage("Poro Walk (11).gif");
    walk[11] = loadImage("Poro Walk (12).gif");
    walk[12] = loadImage("Poro Walk (13).gif");
    walk[13] = loadImage("Poro Walk (14).gif");
    walk[14] = loadImage("Poro Walk (15).gif");
    walk[15] = loadImage("Poro Walk (16).gif");
    for (int i = 0; i < walk.length; i++) {
      walk[i].resize(80, 80);
    }


    grab = new PImage[16];

    grab[0] = loadImage("Poro Grab (1).gif");
    grab[1] = loadImage("Poro Grab (2).gif");
    grab[2] = loadImage("Poro Grab (3).gif");
    grab[3] = loadImage("Poro Grab (4).gif");
    grab[4] = loadImage("Poro Grab (5).gif");
    grab[5] = loadImage("Poro Grab (6).gif");
    grab[6] = loadImage("Poro Grab (7).gif");
    grab[7] = loadImage("Poro Grab (8).gif");
    grab[8] = loadImage("Poro Grab (9).gif");
    grab[9] = loadImage("Poro Grab (10).gif");
    grab[10] = loadImage("Poro Grab (11).gif");
    grab[11] = loadImage("Poro Grab (12).gif");
    grab[12] = loadImage("Poro Grab (13).gif");
    grab[13] = loadImage("Poro Grab (14).gif");
    grab[14] = loadImage("Poro Grab (15).gif");
    grab[15] = loadImage("Poro Grab (16).gif");
    for (int i = 0; i < grab.length; i++) {
      grab[i].resize(80, 80);
    }

    stand = new PImage[16];

    stand[0] = loadImage("Poro Stand (1).gif");
    stand[1] = loadImage("Poro Stand (2).gif");
    stand[2] = loadImage("Poro Stand (3).gif");
    stand[3] = loadImage("Poro Stand (4).gif");
    stand[4] = loadImage("Poro Stand (5).gif");
    stand[5] = loadImage("Poro Stand (6).gif");
    stand[6] = loadImage("Poro Stand (7).gif");
    stand[7] = loadImage("Poro Stand (8).gif");
    stand[8] = loadImage("Poro Stand (9).gif");
    stand[9] = loadImage("Poro Stand (10).gif");
    stand[10] = loadImage("Poro Stand (11).gif");
    stand[11] = loadImage("Poro Stand (12).gif");
    stand[12] = loadImage("Poro Stand (13).gif");
    stand[13] = loadImage("Poro Stand (14).gif");
    stand[14] = loadImage("Poro Stand (15).gif");
    stand[15] = loadImage("Poro Stand (16).gif");

    for (int i = 0; i < stand.length; i++) {
      stand[i].resize(80, 80);
    }


    jump = new PImage[16];

    jump[0] = loadImage("Poro Jump (1).gif");
    jump[1] = loadImage("Poro Jump (2).gif");
    jump[2] = loadImage("Poro Jump (3).gif");
    jump[3] = loadImage("Poro Jump (4).gif");
    jump[4] = loadImage("Poro Jump (5).gif");
    jump[5] = loadImage("Poro Jump (6).gif");
    jump[6] = loadImage("Poro Jump (7).gif");
    jump[7] = loadImage("Poro Jump (8).gif");
    jump[8] = loadImage("Poro Jump (9).gif");
    jump[9] = loadImage("Poro Jump (10).gif");
    jump[10] = loadImage("Poro Jump (11).gif");
    jump[11] = loadImage("Poro Jump (12).gif");
    jump[12] = loadImage("Poro Jump (13).gif");
    jump[13] = loadImage("Poro Jump (14).gif");
    jump[14] = loadImage("Poro Jump (15).gif");
    jump[15] = loadImage("Poro Jump (16).gif");

    for (int i = 0; i < jump.length; i++) {
      jump[i].resize(80, 80);
    }

    grabber = new mouseGrabber(Position, 50, 50); // Create new mouse grabber
    increaseSpeed = 0; // set increaseSpeed to 0
    gravity = 0.2; // setup the gravity
    Position = new PVector(250, 200); // Setup the position
  }



  //Display method
  public void display() {
    grabber.grabber(Position);
    //If the object is being grabbed
    if (grabber.getIsGrabbed() == true) {
      grabber.setPosition(Position); // set the position of the player 
      image(grab[frameCount%16], Position.x, Position.y);
      canJump = false;
      increaseSpeed = 0;
    } else if (grabber.getIsGrabbed() == false) { // If the object is not being grabed
      movePlayer(); // Move the player regularly
      if (left == true) {
        image(walk[frameCount%16], Position.x, Position.y);
      } else if (right == true) {
        image(walk[frameCount%16], Position.x, Position.y);
      } else if(Jump == true){
        image(jump[frameCount%16], Position.x, Position.y);
      }else {
        image(stand[frameCount%16], Position.x, Position.y);
      }
    }
  }


  //This method will be how the player will move
  public void movePlayer() {
    if (right == true) { // If the right boolean is true
      Position.x += movementSpeed; // Move the players x position
    }
    if (left == true) { // If the left position is true
      Position.x -= movementSpeed; // Move the players y position
    }

    //If the player is on an object
    if (onObject == true) {
      //Position.y = yLevelOfBox - 50;
      increaseSpeed = 1;
      canJump = true; // The player can jump
      Jump = false;
    } else if (onObject == false && grabber.getIsGrabbed() == false) { // If the player is not on an object and is not being grabbed
      canJump = false;
      Jump = true;
      if (increaseSpeed >= 7) {
        increaseSpeed = 7;
      }
      Position.y += increaseSpeed; // Decrease the players position
      increaseSpeed += gravity; // Increase the rate of change the player is being deacreased at
    }
  }

  //This is a method will move the player
  public void movement() {
    if (keyCode == 'W' && canJump == true) { // If the player keyCode is W and the player can jump
      increaseSpeed = increaseSpeed * -8; // Make the player jump
      canJump = false; // The player cannot jump in the air
    } 
    if (keyCode == 'A') { // If A key is pressed
      left = true; // The player can go left
    }
    if (keyCode == 'D') { // If D key is pressed
      right = true; // The player can go right
    }
  }

  public void stopPlayer() {
    if (keyCode == 'D') { // If the D key is not pressed
      right = false; // The boolean right is false
    }
    if (keyCode == 'A') { // If the A keyis not pressed
      left = false; // The boolean left is false
    }
  }

  public void reset() {
    Position = new PVector(250, 200);
  }

  //This will set wheather the player is on an object or not
  public void setOnObject(boolean onObject) {
    this.onObject = onObject;
  }

  public boolean getOnObject() {
    return onObject;
  }


  //This will return a players position
  public PVector getPosition() {
    return Position;
  }

  //This will return the increaseSpeed variable
  public float getIncreaseSpeed() {
    return increaseSpeed;
  }

  public int getLife() {
    return life;
  }

  public void setLife(int newLife) {
    life = newLife;
  }
}
